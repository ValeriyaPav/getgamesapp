//
//  GameModelContainer.swift
//  GetGamesApp
//
//  Created by Валерия П on 3.10.21.
//

import Foundation

struct GameModelContainer: Codable {
    
    let results: [GameModel]
}
