//
//  ViewController.swift
//  GetGamesApp
//
//  Created by Валерия П on 3.10.21.
//

import UIKit

class ViewController: UIViewController {
    
    private var gamesArray: [GameModel] = []
    
    @IBOutlet weak var gamesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gamesTableView.dataSource = self
        self.gamesTableView.delegate = self
        
        NetworkManager.getGames(completion: { array in
            if let array = array {
                self.gamesArray = array
                DispatchQueue.main.async {
                    self.gamesTableView.reloadData()
                }
            }
        })
        
        self.gamesTableView.register(UINib(nibName: GameTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: GameTableViewCell.reuseId)
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GameTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GameTableViewCell = self.gamesTableView.dequeueReusableCell(withIdentifier: GameTableViewCell.reuseId, for: indexPath) as! GameTableViewCell
        cell.configureWithModel(model: gamesArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gamesArray.count
    }
}
