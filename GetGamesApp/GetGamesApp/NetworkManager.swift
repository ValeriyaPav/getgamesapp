//
//  NetworkManager.swift
//  GetGamesApp
//
//  Created by Валерия П on 3.10.21.
//

import Foundation

class NetworkManager {
    
    static private let appKey: String = "055d147f7cb3403b8cf35257db9c3d53"
    
    static private var urlString: String {
        "https://api.rawg.io/api/games?key=\(appKey)"
    }
    static private var urlDeveloperString: String {
        "https://api.rawg.io/api/developers?key=\(appKey)"
    }
    
    static func getGames(completion: @escaping ([GameModel]?) -> ()) {
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let urlSession: URLSession = URLSession.shared
            urlSession.dataTask(with: request, completionHandler: { data, response, error in
                if let error = error {
                    print(error.localizedDescription)
                }
                if let data = data {
                    do {
                        let container: GameModelContainer = try JSONDecoder().decode(GameModelContainer.self, from: data)
                        completion(container.results)
                    } catch {
                        print(error)
                    }
                }
            })
                .resume()
        }
    }
    
    static func getDevelopers(completion: @escaping ([DeveloperModel]?) -> ()) {
        if let url = URL(string: urlDeveloperString) {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let urlSession: URLSession = URLSession.shared
            urlSession.dataTask(with: request, completionHandler: { data, response, error in
                if let error = error {
                    print(error.localizedDescription)
                }
                if let data = data {
                    do {
                        let container: DeveloperModelContainer = try JSONDecoder().decode(DeveloperModelContainer.self, from: data)
                        completion(container.results)
                    } catch {
                        print(error)
                    }
                }
            })
                .resume()
        }
    }
}
