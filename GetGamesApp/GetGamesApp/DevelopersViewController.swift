//
//  DevelopersViewController.swift
//  GetGamesApp
//
//  Created by Валерия П on 5.10.21.
//

import UIKit

class DevelopersViewController: UIViewController {
    
    @IBOutlet weak var developersTableView: UITableView!
    
    private var developersArray: [DeveloperModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.developersTableView.dataSource = self
        self.developersTableView.delegate = self
        
        NetworkManager.getDevelopers(completion: { array in
            if let array = array {
                self.developersArray = array
                DispatchQueue.main.async {
                    self.developersTableView.reloadData()
                }
            }
        })
        
        self.developersTableView.register(UINib(nibName: DeveloperTableViewCell.reuseId, bundle: nil), forCellReuseIdentifier: DeveloperTableViewCell.reuseId)
    }
    
    
}

extension DevelopersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GameTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DeveloperTableViewCell = self.developersTableView.dequeueReusableCell(withIdentifier: DeveloperTableViewCell.reuseId, for: indexPath) as! DeveloperTableViewCell
        cell.configureWithModel(model: developersArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return developersArray.count
    }
}
