//
//  GameTableViewCell.swift
//  GetGamesApp
//
//  Created by Валерия П on 3.10.21.
//

import UIKit
import Kingfisher

class GameTableViewCell: UITableViewCell {
    
    static let reuseId: String = "GameTableViewCell"
    static let cellHeight: CGFloat = 70.0
    
    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var gameImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureWithModel(model: GameModel) {
        self.gameNameLabel.text = model.name
        if let urlString = model.background_image {
            let url = URL(string: urlString)
            gameImageView.kf.setImage(with: url)
        }
    }
    
}
