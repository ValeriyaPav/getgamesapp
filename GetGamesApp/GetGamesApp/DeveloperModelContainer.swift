//
//  DeveloperModelContainer.swift
//  GetGamesApp
//
//  Created by Валерия П on 5.10.21.
//

import Foundation

struct DeveloperModelContainer: Codable {
    
    let results: [DeveloperModel]
}
