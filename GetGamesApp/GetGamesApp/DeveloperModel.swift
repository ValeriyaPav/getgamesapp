//
//  DeveloperModel.swift
//  GetGamesApp
//
//  Created by Валерия П on 5.10.21.
//

import Foundation

struct DeveloperModel: Codable {
    let id: Double
    let name: String
    let image_background: String?
}
