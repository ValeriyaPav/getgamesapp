//
//  GameModel.swift
//  GetGamesApp
//
//  Created by Валерия П on 3.10.21.
//

import Foundation

struct GameModel: Codable {
    let id: Double
    let name: String
    let background_image: String?
}
