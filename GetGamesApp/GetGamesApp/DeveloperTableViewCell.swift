//
//  DeveloperTableViewCell.swift
//  GetGamesApp
//
//  Created by Валерия П on 5.10.21.
//

import UIKit

class DeveloperTableViewCell: UITableViewCell {
    
    @IBOutlet weak var developerImageView: UIImageView!
    
    @IBOutlet weak var developersLabel: UILabel!
    
    static let reuseId: String = "DeveloperTableViewCell"
    static let cellHeight: CGFloat = 70.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureWithModel(model: DeveloperModel) {
        self.developersLabel.text = model.name
        if let urlString = model.image_background {
            let url = URL(string: urlString)
            developerImageView.kf.setImage(with: url)
        }
    }
    
}
