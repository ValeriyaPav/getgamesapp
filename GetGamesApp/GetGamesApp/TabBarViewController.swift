//
//  TabBarViewController.swift
//  GetGamesApp
//
//  Created by Валерия П on 5.10.21.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.items?[0].title = "Games"
        tabBar.items?[1].title = "Developers"
    }

}
